#!/bin/bash

# Runs all other scripts

./modify_storage.sh
./create_users.sh
./build_gitlab_runner.sh
# no setup_runner.sh; it's used in build_gitlab_runner.sh