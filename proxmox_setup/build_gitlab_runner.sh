#!/bin/bash

CT_ID=199
CORES=2
MEM=512
IP=192.168.1.99
GW=192.168.1.1
HD=16
NAME=gitlab-runner
BRIDGE=vmbr0


echo "Stopping old container if exists"
pct stop "${CT_ID}"
echo "Waiting 2 sec for container to die"
sleep 2

echo "Deleting old container if exists"
pct destroy "${CT_ID}"

echo "Downloading Rocky9 template"
template=$(pveam available | grep rockylinux-9)
template="${template// /}"
template="${template#system}"
pveam download local "${template}"


echo "Building new container"
pct create "${CT_ID}" local:vztmpl/"${template}" \
--cores "${CORES}" \
--hostname "${NAME}" \
--memory "${MEM}" \
--net0 name=eth0,bridge="${BRIDGE}",firewall=1,gw="${GW}",ip="${IP}"/24,type=veth \
--storage local \
--rootfs local-zfs:"${HD}" \
--unprivileged 1 \
--ostype centos \
--start 1 \
--onboot 1 \
--features nesting=1


#copy over software/secret scripts INTO the LXC via PCT push command
pct push "${CT_ID}" $(dirname "$0")/setup_runner.sh /root/setup_runner.sh

#execute the chmod command inside the LXC to make .sh files exc
pct exec "${CT_ID}" chmod +x /root/setup_runner.sh

#execute the software.sh file inside the LXC run install processes and such.
pct exec "${CT_ID}" /root/setup_runner.sh

#reboot the LXC one last time, so services start up and we can make sure things work.
pct reboot "${CT_ID}"