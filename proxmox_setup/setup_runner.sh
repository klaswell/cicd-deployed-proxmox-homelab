#!/bin/bash

dnf update -y

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash

dnf install gitlab-runner -y


dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
dnf install docker-ce docker-ce-cli containerd.io -y
systemctl start docker
systemctl enable docker


echo
read -p "Enter Gitlab registration token: " token

echo "Registering Gitlab runner"
gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "${token}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "docker,homelab" \
  --run-untagged="true" \
  --locked="false" \
  --limit 2 \
  --request-concurrency 2 \
  --access-level="not_protected"

sed -i 's/concurrent = 1/concurrent = 2/' /etc/gitlab-runner/config.toml
systemctl restart gitlab-runner