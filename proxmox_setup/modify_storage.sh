#!/bin/bash

# Adds 'Snippets' as allowed content on 'local' storage
# This allows for saving cloud-init files onto 'local'

pvesm set local --content images,rootdir,vztmpl,backup,iso,snippets