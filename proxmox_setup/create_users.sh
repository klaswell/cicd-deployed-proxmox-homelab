#!/bin/bash

# Interactive script for setting up Pulumi user in Proxmox

# Creating group in proxmox
echo "Creating group"
pveum group add IAC

# Create user in proxmox
echo "Creating pulumi@pve user"
pveum user add pulumi@pve -group IAC
pveum passwd pulumi@pve

# Creating permissions in proxmox
echo "Adding permissions to group"
pveum acl modify /storage -group IAC -role PVEDatastoreAdmin
pveum acl modify /vms -group IAC -role PVEVMAdmin