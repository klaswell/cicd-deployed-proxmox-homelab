wget -nc -P /var/lib/vz/template/qcow/ "${CLOUD_IMAGE_URL}"
cd /var/lib/vz/template/qcow
qm create "${TEMPLATE_ID}" \
    --name "${TEMPLATE_NAME}" \
    --cpu cputype=host \
    --memory 1024 \
    --net0 virtio,bridge=vmbr0 \
    --scsihw virtio-scsi-pci \
    --agent enabled=1
qm set "${TEMPLATE_ID}" \
    --scsi0 local-zfs:0,import-from=/var/lib/vz/template/qcow/"${CLOUD_IMAGE_NAME}"
qm set "${TEMPLATE_ID}" \
    --ide2 local-zfs:cloudinit
qm set "${TEMPLATE_ID}" --boot order=scsi0
qm set "${TEMPLATE_ID}" \
    --cicustom "user=local:snippets/${TEMPLATE_NAME}-user-data,meta=local:snippets/${TEMPLATE_NAME}-meta-data"
qm start "${TEMPLATE_ID}"
qm wait "${TEMPLATE_ID}" # waits until machine is shut down (happens in cloud-init file)
qm template "${TEMPLATE_ID}"