## Overview

This project creates the ability to stand up a Proxmox-based homelab, consisting of several VM's, from a cloud-hosted GitLab CI/CD pipeline. No other machines outside of the Proxmox server itself are required.

A series of scripts will first be ran on Proxmox to set it up, create a local GitLab runner LXC container that will execute your pipeline, and create a Pulumi user.

After the initial Proxmox setup, running the pipeline will then use cloud images to create two VM templates: one for Rocky Linux 9 and one for Debian 11. Cloud-init files will do some basic setup: setting up user/password, SSH keys, installing a few basic packages, and finally shutting down the machines to become customized, ready-to-go, VM templates.

The final job of the pipeline will run 'pulumi up'. The Pulumi code (written in Python) will create new VM's by full cloning each template. Some example code is included that copies files to the machines for execution or runs commands on the machines. More specifically, a script to install OpenMediaVault is copied/executed on the new Debian machine, and some basic Docker install/setup is done on the Rocky machine. The example code should be fairly simple to reverse engineer and adapt for your own purposes.


## How To Use

- Copy/clone this repo to your local Proxmox machine
- Ensure all scripts in /proxmox_setup are executable (chmod +x)
- Have a password for a new 'pulumi' user ready
- Have your GitLab runner token ready
- ./run.sh
- Clone repo to hosted GitLab
- Create variables listed below
- Ensure public SSH key has been added to Proxmox
- Modify cloud-init files in /proxmox_templates to your liking
- Run pipeline


## Required GitLab Variables

| Variable                | Description |
| ----------------------- | ----------- |
| PROXMOX_URL             | simple IP e.g. 192.168.1.7; used in pipeline jobs for SSH |
| PROXMOX_USERNAME_PULUMI | Proxmox user created via setup script (e.g. pulumi@pve) |
| PROXMOX_PASSWORD_PULUMI | Proxmox user's password created via setup script |
| PROXMOX_URL_PULUMI      | full Proxmox URL including port e.g. https://192.168.1.7:8006 |
| PULUMI_ACCESS_TOKEN     | token for using Pulumi |
| SSH_PRIVATE_KEY	      | private key for SSH'ing into Proxmox; must add Public Key to Proxmox also |
| USER                    | user to create in templates |