"""Homelab Pulumi in Python"""

import pulumi
import pulumi_proxmoxve as proxmox
import pulumi_command as command
import os
import subprocess

USER = os.environ["USER"]
PASSWORD = os.environ["PASSWORD"]


provider = proxmox.Provider("proxmoxve",
    virtual_environment = {
        "endpoint": os.environ["PROXMOX_URL_PULUMI"],
        "insecure": "true",
        "username": os.environ["PROXMOX_USERNAME_PULUMI"],
        "password": os.environ["PROXMOX_PASSWORD_PULUMI"]
    }
)

args = {}


omv = proxmox.vm.VirtualMachine("omv",
    node_name = "proxmox",
    opts = pulumi.ResourceOptions(provider = provider),
    agent = proxmox.vm.VirtualMachineAgentArgs(
        enabled = True,
        trim = True,
        type = "virtio"
    ),
    cpu = proxmox.vm.VirtualMachineCpuArgs(
        cores = 2,
        type = "host"
    ),
    clone = proxmox.vm.VirtualMachineCloneArgs(
        vm_id = 8000,
        full = True
    ),
    disks = [
        proxmox.vm.VirtualMachineDiskArgs(
            interface = "scsi0",
            datastore_id = "local-zfs",
            size = 16
        )
    ],
    memory = proxmox.vm.VirtualMachineMemoryArgs(
        dedicated = 2048
    ),
    name = "omv",
    on_boot = True,
    operating_system = proxmox.vm.VirtualMachineOperatingSystemArgs(
        type = "l26"
    )
)


rocky_main = proxmox.vm.VirtualMachine("rocky_main",
    node_name = "proxmox",
    opts = pulumi.ResourceOptions(provider = provider),
    agent = proxmox.vm.VirtualMachineAgentArgs(
        enabled = True,
        trim = True,
        type = "virtio"
    ),
    cpu = proxmox.vm.VirtualMachineCpuArgs(
        cores = 2,
        type = "host"
    ),
    clone = proxmox.vm.VirtualMachineCloneArgs(
        vm_id = 9000,
        full = True
    ),
    disks = [
        proxmox.vm.VirtualMachineDiskArgs(
            interface = "scsi0",
            datastore_id = "local-zfs",
            size = 50
        )
    ],
    memory = proxmox.vm.VirtualMachineMemoryArgs(
        dedicated = 2048
    ),
    name = "rocky-main",
    on_boot = True,
    operating_system = proxmox.vm.VirtualMachineOperatingSystemArgs(
        type = "l26"
    )
)


rocky_main_ip = rocky_main.ipv4_addresses[1][0]
omv_ip = omv.ipv4_addresses[1][0]

pulumi.export("rocky_main_ip", rocky_main_ip)
pulumi.export("omv_ip", omv_ip)

rocky_main_connection = command.remote.ConnectionArgs(
    host = rocky_main_ip,
    user = USER,
    password = PASSWORD,
#    private_key = os.environ["SSH_PRIVATE_KEY"]
)

omv_connection = command.remote.ConnectionArgs(
    host = omv_ip,
    user = USER,
    password = PASSWORD,
#    private_key = os.environ["SSH_PRIVATE_KEY"]
)


# Rocky setup

command.remote.Command("update_rocky_main",
    connection = rocky_main_connection,
    create = "sudo dnf update -y"
)

command.remote.Command("setup_docker_rocky_main",
    connection = rocky_main_connection,
    create = "sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo; \
        sudo dnf install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin; \
        sudo systemctl start docker; \
        sudo systemctl enable docker; \
        sudo usermod -aG docker " + USER
)

command.remote.Command("install_s3cmd_rocky_main",
    connection = rocky_main_connection,
    create = "sudo dnf install -y python python-pip; \
        pip install s3cmd;"
)


# OMV setup

command.remote.Command("update_omv",
    connection = omv_connection,
    create = "sudo apt-get update -y"
)

FILE = os.environ["CI_PROJECT_DIR"] + "/infrastructure/omv/scripts/install_omv.sh"

copy_file = command.remote.CopyFile("copy_install_omv",
    connection = omv_connection,
    local_path = FILE,
    remote_path = "install_omv.sh"
)

# installing breaks SSH; make sure this is last
# DNS also needs setup in omv
install_omv = command.remote.Command("install_omv",
    connection = omv_connection,
    create = "sudo /bin/bash install_omv.sh",
    opts = pulumi.ResourceOptions(depends_on = copy_file)
)